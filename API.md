#These API endpoints are available now

**Branches**:

* `/branches` (**GET**) - returns all branches data (json)
* `/branches/city/{name}` (**GET**) - returns one branch data (json) searching by the city name; replace `{name}` with one of the following: Rotterdam, Madrid, Munich
* `/branches/{id}` (**GET**) - returns one branch data (json) searching by the branch id; replace `{id}` with one of the following: B001, B002, B003
* `/branches/{id}/manager` (**GET**) - returns the manager data (json) for the selected branch; see above for `{id}`
* `/branches` (**POST**) - accepts POST data and saves a new branch to the database

**Employees**:

* `/employees` (**GET**) - returns all employees data (json)
* `/employees/{id}` (**GET**) - returns one employee data (json) searching by the employee id; replace `{id}` with one of the employee ids in `employee.json`
* `/employees/managers` (**GET**) - returns all employees set as branch managers (json)
* `/employees/managedby/{id}` (**GET**) - returns an array of employees that are managed by the branch manager with the specified `{id}`
* `/employees` (**POST**) - accepts POST data and saves a new employee to the database
