#Node.js & Express Training Application

##Prerequisites

- Node.js version 4 LTS - download installer from [Node.js website](http://nodejs.org/en/download/)
- NPM version 2 or above - comes with the Node.js install
- Express version 4 (install with `npm install -g express express-generator`)

##Initial setup (Windows)

- Install all prerequisites from above
- Open the Node.js command prompt from the Start menu and type these commands
(the `\>` symbol represents the prompt; replace {workdir} with any relevant name)
```
\> d:
\> mkdir {workdir}
\> cd {workdir}
```

- Clone the current application code with `git clone https://bitbucket.org/marius_stuparu/node.js-training.git`
- **OR** run the `express` generator

```
\> express

   create : .
   create : ./package.json
   create : ./app.js
   create : ./public/javascripts
   create : ./public/images
   create : ./public
   create : ./public/stylesheets
   create : ./public/stylesheets/style.css
   create : ./routes
   create : ./routes/index.js
   create : ./routes/users.js
   create : ./views
   create : ./views/index.jade
   create : ./views/layout.jade
   create : ./views/error.jade
   create : ./bin
   create : ./bin/www

   install dependencies:
     $ cd . && npm install

   run the app:
     $ DEBUG=node-training:* npm start
```

- Install all dependencies with `npm install`

Wait for NPM to install all required packages. When it finishes, use this command to start the Express app:

```
\> DEBUG=node-training:* npm start
```

When you see this output: `node-training:server Listening on port 3000` you can navigate your browser to this address: `http://localhost:3000` to see the application.
