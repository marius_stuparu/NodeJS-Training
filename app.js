var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

/**
 * The view engine setup.
 * Everything in the views folder will be considered a template,
 * and the templating engine will be Jade http://jade-lang.com/reference/
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ limit: '10mb' }));
app.use(cookieParser());
/**
 * The line below allows access to the public folder contents as static files
 */
app.use(express.static(path.join(__dirname, 'public')));

/**
 * This is where we define the controllers for our routes
 * You can ommit the .js extension from `required` modules
 */
var routes = require('./routes/index');
var employees = require('./routes/employees');
var branches = require('./routes/branches');
var newbranch = require('./routes/new-branch');
var newemployee = require('./routes/new-employee');

/**
 * These are our route listeners.
 * Each access of these URLs will be handled by the controller we required above
 */
app.use('/', routes);
app.use('/employees', employees);
app.use('/branches', branches);
app.use('/new/branch', newbranch);
app.use('/new/employee', newemployee);
/**
 * No more editing after this line
 */

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
