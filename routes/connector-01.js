var branches  = require('./branches.json');
var employees = require('./employees.json');

var connector = {
    db: {
        branches: branches,
        employees: employees 
    }
};

module.exports = connector;
