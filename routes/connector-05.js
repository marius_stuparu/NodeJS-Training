var _ = require('lodash');
var fs = require('fs');

var connector = {
    db: {}
};

/**
 * Private file system helpers
 */
function readDatabases() {
    var success;

    try {
        var branches_json  = JSON.parse( fs.readFileSync(__dirname + '/branches.json', 'utf8') );
        var employees_json = JSON.parse( fs.readFileSync(__dirname + '/employees.json', 'utf8') );

        connector.db.branches = branches_json;
        connector.db.employees = employees_json;

        success = true;
    } catch (err) {
        console.error(err);
        success = false;
    }

    return success;
}

function writeDatabase(database) {
    var success;
    var document;

    switch( database ) {
        case 'branches': document = connector.db.branches; break;
        case 'employees': document = connector.db.employees; break;
    }

    try {
        fs.writeFileSync(__dirname + '/' + database + '.json', JSON.stringify(document, null, 4), 'utf8');

        success = true;
    } catch (err) {
        console.error(err);
        success = false;
    }

    return success;
}

/**
 * Public database methods
 */
connector.db.find = function(where, filters) {
    var document;

    if( readDatabases() ) {
        switch( where ) {
            case 'branches': document = connector.db.branches; break;
            case 'employees': document = connector.db.employees; break;
        }

        return _.find(document, _.matches(filters));
    } else {
        return null;
    }
}

connector.db.findAll = function(where, filters) {
    var document;

    if( readDatabases() ) {
        switch( where ) {
            case 'branches': document = connector.db.branches; break;
            case 'employees': document = connector.db.employees; break;
        }

        return _.filter(document, _.matches(filters));
    } else {
        return null;
    }
}

connector.db.insert = function(where, newObject) {
    var document;

    if( readDatabases() ) {
        switch( where ) {
            case 'branches': document = connector.db.branches; break;
            case 'employees': document = connector.db.employees; break;
        }

        document.push(newObject);
        
        if(writeDatabase(where)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

module.exports = connector;
