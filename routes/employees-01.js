var express = require('express');
var router = express.Router();

var connector = require('./connector');

router.get('/', function(req, res, next) {
    res.json(connector.db.employees);
});

module.exports = router;
