var express = require('express');
var router = express.Router();

var connector = require('./connector');

router.get('/', function(req, res, next) {
    res.json(connector.db.findAll('employees'));
});

router.get('/managers', function(req, res, next) {
    res.json(connector.db.findAll('employees', {'is_branch_manager': true}));
});

router.get('/managedby/:id', function(req, res, next) {
    var employee_id = req.params.id;
    res.json(connector.db.findAll('employees', {'manager_id': employee_id}));
});

router.get('/:id', function(req, res, next) {
    var employee_id = req.params.id;
    res.json(connector.db.find('employees', {'id': employee_id}));
});

module.exports = router;
