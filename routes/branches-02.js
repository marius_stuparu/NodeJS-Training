var express = require('express');
var router = express.Router();

var connector = require('./connector');

router.get('/', function(req, res, next) {
    res.json(connector.db.branches);
});

router.get('/city/:name', function(req, res, next) {
    var city_name = req.params.name;
    res.json(connector.db.find('branches', {'city': city_name}));
});

router.get('/:id/manager', function(req, res, next) {
    var branch_id = req.params.id;
    res.json(connector.db.find('employees', {'branch_id': branch_id, 'is_branch_manager': true}));
});

router.get('/:id', function(req, res, next) {
    var branch_id = req.params.id;
    res.json(connector.db.find('branches', {'id': branch_id}));
});

module.exports = router;
