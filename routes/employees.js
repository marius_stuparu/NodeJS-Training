/**
 * Lodash is a helper library.
 * More documentation here https://lodash.com/docs
 */
var _ = require('lodash');
var express = require('express');
var router = express.Router();
var fs = require('fs');

/**
 * This is the module responsible with all database access.
 * In real world applications, this will be a module that
 * connects us to a database server like MySQL, Oracle or Mongo.
 */
var connector = require('./connector');

/**
 * Default route; we listen to GET requests on the /employees/ URL
 * @param  {Object}     req   the Node http request object
 * @param  {Object}     res   the Node http response object
 * @param  {function}   next will be discussed later
 */
router.get('/', function(req, res, next) {
    res.json(connector.db.findAll('employees'));
});

/**
 * Simple URL to retrieve only employees that have the `is_branch_manager`
 * flag set to true. As this database was designed, there can be only one
 * manager per branch
 * @return {array}       An array containing objects of all managers
 */
router.get('/managers', function(req, res, next) {
    res.json(connector.db.findAll('employees', {'is_branch_manager': true}));
});

/**
 * Get all employees under the manager with the given unique id
 * :id is a parameter in the URL that is saved in the req.params object
 * and can be used inside this route controller.
 * Order of parameters inside the URL is irrelevant, as long as they are
 * properly sepparated to avoid confusions.
 */
router.get('/managedby/:id', function(req, res, next) {
    var employee_id = req.params.id;
    res.json(connector.db.findAll('employees', {'manager_id': employee_id}));
});

/**
 * Get the employee details based on his/her unique id
 */
router.get('/:id', function(req, res, next) {
    var employee_id = req.params.id;
    res.json(connector.db.find('employees', {'id': employee_id}));
});

/**
 * The POST listener accepts data sent from a web form
 * @param  {Object} req   the request object contains the form data inside the req.body 
 */
router.post('/', function(req, res, next) {
    var post_data = req.body;
    var branch_indicator = '';
    var manager_id = '';

    var image_base64 = post_data.avatar;
    var image_type = image_base64.match(/^data\:image\/png/) ? 'png' : 'jpg';
    var image_data = '';

    switch(image_type) {
        case 'png': image_data = image_base64.replace(/^data\:image\/png,/, ''); break;
        case 'jpg': image_data = image_base64.replace(/^data\:image\/jpe?g,/, ''); break;
    }

    switch(post_data.branch_id) {
        case 'B001': branch_indicator = 'ROT1'; manager_id = 'ROT001'; break;
        case 'B002': branch_indicator = 'MAD1'; manager_id = 'MAD001'; break;
        case 'B003': branch_indicator = 'MUN1'; manager_id = 'MUN001'; break;
        default:     branch_indicator = 'UNK1'; manager_id = 'UNK001'; break;
    }

    var constructed_object = {
        id: _.uniqueId(branch_indicator),
        name: post_data.name,
        email: post_data.email,
        hire_date: post_data.hire_date,
        manager_id: manager_id,
        is_branch_manager: post_data.is_manager,
        branch_id: post_data.branch_id
    }

    fs.writeFile('./public/images/' + constructed_object.id + '.' + image_type, image_data, 'base64', function(err){
        if (err) throw err;

        constructed_object.avatar = '/images/' + constructed_object.id + '.' + image_type;

        /**
         * We use the insert() function to add a new object to the 'employees' database.
         * If the function returns true, the insert was successful, otherwise we
         * send a server error 500
         */
        if( connector.db.insert('employees', constructed_object) ) {
            res.status(200).end();
        } else {
            res.status(500).end();
        }
    })
});

module.exports = router;
