/**
 * Lodash is a helper library.
 * More documentation here https://lodash.com/docs
 */
var _ = require('lodash');

/**
 * fs is the internal Node library that can handle file system operations
 * More info here https://nodejs.org/dist/latest-v4.x/docs/api/fs.html
 */
var fs = require('fs');

/**
 * We set up the connector object, to be populated later with
 * data from the json files and access methods.
 * @type {Object}
 */
var connector = {
    db: {}
};

/**
 * Private file system helpers
 */
/**
 * Tries to read the two json files from the file system and
 * assign them in the connector.db object.
 * @return {boolean} If the database is successfully populated, return true
 */
function readDatabases() {
    var success;

    try {
        /**
         * Because the json files are relatively small, we can afford to use the synchronized
         * ead/write for simplicity, but it is indicated to use the asynchronous forms.
         */
        var branches_json  = JSON.parse( fs.readFileSync(__dirname + '/branches.json', 'utf8') );
        var employees_json = JSON.parse( fs.readFileSync(__dirname + '/employees.json', 'utf8') );

        connector.db.branches = branches_json;
        connector.db.employees = employees_json;

        success = true;
    } catch (err) {
        console.error(err);
        success = false;
    }

    return success;
}

/**
 * Write the currently in-memory databases back to the file system to persist them.
 * @param  {string} database  In out case, wither 'branches' or 'employees', the name part of the files
 * @return {boolean}          If the save was successful, return true
 */
function writeDatabase(database) {
    var success;
    var document;

    switch( database ) {
        case 'branches': document = connector.db.branches; break;
        case 'employees': document = connector.db.employees; break;
    }

    try {
        /**
         * Because the json files are relatively small, we can afford to use the synchronized
         * ead/write for simplicity, but it is indicated to use the asynchronous forms.
         */
        fs.writeFileSync(__dirname + '/' + database + '.json', JSON.stringify(document, null, 4), 'utf8');

        success = true;
    } catch (err) {
        console.error(err);
        success = false;
    }

    return success;
}

/**
 * Public database methods
 */
/**
 * Finds _one_ object that matches the filter in the given database
 * @param  {string} where   in which database to search for
 * @param  {Object} filters an object with attributes to match
 * @return {Object}         if a matching item was found, returns it
 */
connector.db.find = function(where, filters) {
    var document;

    if( readDatabases() ) {
        switch( where ) {
            case 'branches': document = connector.db.branches; break;
            case 'employees': document = connector.db.employees; break;
        }

        /**
         * Lodash provides very fast to implement functions for our needs
         */
        return _.find(document, _.matches(filters));
    } else {
        return null;
    }
}

/**
 * Finds _multiple_ or all objects that match the filter in the given database
 * @param  {string} where   in which database to search for
 * @param  {Object} filters an object with attributes to match
 * @return {Object}         if matching items were found, returns them in an array
 */
connector.db.findAll = function(where, filters) {
    var document;

    if( readDatabases() ) {
        switch( where ) {
            case 'branches': document = connector.db.branches; break;
            case 'employees': document = connector.db.employees; break;
        }

        /**
         * _.filter is different from _.find in that it can retrieve multiple hits
         */
        return _.filter(document, _.matches(filters));
    } else {
        return null;
    }
}

/**
 * Adds a new item to the selected in-memory database, and calls the save-to-disk function
 * @param  {string} where     in which database to add
 * @param  {Object} newObject the new item to be added
 * @return {boolean}          if the save was successful, return true
 */
connector.db.insert = function(where, newObject) {
    var document;
    var status;

    if( readDatabases() ) {
        switch( where ) {
            case 'branches': document = connector.db.branches; break;
            case 'employees': document = connector.db.employees; break;
        }

        /**
         * Since both databases are arrays of objects, we can use the array push()
         */
        document.push(newObject);

        if(writeDatabase(where)) {
            status = true;
        } else {
            status = false;
        }
    } else {
        status = false;
    }

    return status;
}

module.exports = connector;
