var _ = require('lodash');

var branches  = require('./branches.json');
var employees = require('./employees.json');

var connector = {
    db: {
        branches: branches,
        employees: employees 
    }
};

connector.db.find = function(where, filters) {
    var document;

    switch( where ) {
        case 'branches': document = connector.db.branches; break;
        case 'employees': document = connector.db.employees; break;
    }

    return _.find(document, _.matches(filters));
}

connector.db.findAll = function(where, filters) {
    var document;

    switch( where ) {
        case 'branches': document = connector.db.branches; break;
        case 'employees': document = connector.db.employees; break;
    }

    return _.filter(document, _.matches(filters));
}

module.exports = connector;
