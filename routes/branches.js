/**
 * Lodash is a helper library.
 * More documentation here https://lodash.com/docs
 */
var _ = require('lodash');
var express = require('express');
var router = express.Router();

/**
 * This is the module responsible with all database access.
 * In real world applications, this will be a module that
 * connects us to a database server like MySQL, Oracle or Mongo.
 */
var connector = require('./connector');

/**
 * Default route; we listen to GET requests on the /branches/ URL
 * @param  {Object}     req   the Node http request object
 * @param  {Object}     res   the Node http response object
 * @param  {function}   next will be discussed later
 */
router.get('/', function(req, res, next) {
    res.json(connector.db.findAll('branches'));
});

/**
 * Get a branch by the city name.
 * :name is a parameter in the URL that is saved in the req.params object
 * and can be used inside this route controller.
 * Order of parameters inside the URL is irrelevant, as long as they are
 * properly sepparated to avoid confusions.
 */
router.get('/city/:name', function(req, res, next) {
    var city_name = req.params.name;
    /**
     * the 'branches' string is the `where` parameter inside the find() function
     * and the json object {'city': city_name} will become the `filter` parameter
     */
    res.json(connector.db.find('branches', {'city': city_name}));
});

/**
 * GET the manager details for a specified branch
 * Order of the route listeners IS very important, because only
 * the first hit will be satisfied. For example, if the routes were listed like
 * /:id
 * /:id/manager
 * the router would never had passed after the first line, and it would have
 * considered in a request like /branches/B001/manager that
 * req.params.id = 'B001/manager' which would generate an empty response.
 * List the routes from the most specific to the most generic to avoid head scratching
 */
router.get('/:id/manager', function(req, res, next) {
    var branch_id = req.params.id;
    /**
     * Complex filters can be constructed, because lodash allows us to do that
     */
    res.json(connector.db.find('employees', {'branch_id': branch_id, 'is_branch_manager': true}));
});

/**
 * Get the branch details based on its unique id
 */
router.get('/:id', function(req, res, next) {
    var branch_id = req.params.id;
    res.json(connector.db.find('branches', {'id': branch_id}));
});

/**
 * The POST listener accepts data sent from a web form
 * @param  {Object} req   the request object contains the form data inside the req.body 
 */
router.post('/', function(req, res, next) {
    var post_data = req.body;

    var constructed_object = {
        id: _.uniqueId('B1'),
        city: post_data.city,
        geolocation: [
            _.toNumber(post_data.latitude),
            _.toNumber(post_data.longitude)
        ]
    }

    /**
     * We use the insert() function to add a new object to the 'branches' database.
     * If the function returns true, the insert was successful, otherwise we
     * send a server error 500
     */
    if( connector.db.insert('branches', constructed_object) ) {
        res.status(200).end();
    } else {
        res.status(500).end();
    }
});

module.exports = router;
