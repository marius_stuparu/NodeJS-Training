var express = require('express');
var router = express.Router();

/* GET add new branch page. */
router.get('/', function(req, res, next) {
  res.render('addbranch', { title: 'Add another branch' });
});

module.exports = router;
