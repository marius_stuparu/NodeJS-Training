var _ = require('lodash');
var fs = require('fs');

var branches  = JSON.parse( fs.readFileSync('./branches.json', 'utf8') );
var employees = JSON.parse( fs.readFileSync('./employees.json', 'utf8') );

var connector = {
    db: {
        branches: branches,
        employees: employees 
    }
};

connector.db.find = function(where, filters) {
    var document;

    switch( where ) {
        case 'branches': document = connector.db.branches; break;
        case 'employees': document = connector.db.employees; break;
    }

    return _.find(document, _.matches(filters));
}

connector.db.findAll = function(where, filters) {
    var document;

    switch( where ) {
        case 'branches': document = connector.db.branches; break;
        case 'employees': document = connector.db.employees; break;
    }

    return _.filter(document, _.matches(filters));
}

module.exports = connector;
