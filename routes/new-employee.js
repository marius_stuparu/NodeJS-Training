var express = require('express');
var router = express.Router();

/* GET add new employee page. */
router.get('/', function(req, res, next) {
  res.render('addemployee', { title: 'Add another employee' });
});

module.exports = router;
