var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    /**
     * Render the index template from the views folder, and pass the title variable
     * @type {String}
     */
    res.render('index', { title: 'Welcome to Node.js Basics' });
});

module.exports = router;
