var _ = require('lodash');
var express = require('express');
var router = express.Router();

var connector = require('./connector');

router.get('/', function(req, res, next) {
    res.json(connector.db.findAll('employees'));
});

router.get('/managers', function(req, res, next) {
    res.json(connector.db.findAll('employees', {'is_branch_manager': true}));
});

router.get('/managedby/:id', function(req, res, next) {
    var employee_id = req.params.id;
    res.json(connector.db.findAll('employees', {'manager_id': employee_id}));
});

router.get('/:id', function(req, res, next) {
    var employee_id = req.params.id;
    res.json(connector.db.find('employees', {'id': employee_id}));
});

router.post('/', function(req, res, next) {
    var post_data = req.body;

    var constructed_object = {
        id: _.uniqueId(post_data.branch_indicator),
        name: post_data.name,
        email: post_data.email,
        avatar: 'NOT IMPLEMENTED YET',
        hire_date: post_data.hire_date,
        manager_id: post_data.manager_id,
        is_branch_manager: false,
        branch_id: post_data.branch_id
    }

    if( connector.db.insert('employees', constructed_object) ) {
        res.status(201).end();
    } else {
        res.status(500).end();
    }
});

module.exports = router;
