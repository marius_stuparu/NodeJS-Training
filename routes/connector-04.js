var _ = require('lodash');
var fs = require('fs');

var connector = {
    db: {}
};

function readDatabases() {
    var success;

    try {
        var branches_json  = JSON.parse( fs.readFileSync(__dirname + '/branches.json', 'utf8') );
        var employees_json = JSON.parse( fs.readFileSync(__dirname + '/employees.json', 'utf8') );

        connector.db.branches = branches_json;
        connector.db.employees = employees_json;

        success = true;
    } catch (err) {
        console.error(err);
        success = false;
    }

    return success;
}

connector.db.find = function(where, filters) {
    var document;

    if( readDatabases() ) {
        switch( where ) {
            case 'branches': document = connector.db.branches; break;
            case 'employees': document = connector.db.employees; break;
        }

        return _.find(document, _.matches(filters));
    } else {
        return null;
    }
}

connector.db.findAll = function(where, filters) {
    var document;

    if( readDatabases() ) {
        switch( where ) {
            case 'branches': document = connector.db.branches; break;
            case 'employees': document = connector.db.employees; break;
        }

        return _.filter(document, _.matches(filters));
    } else {
        return null;
    }
}

module.exports = connector;
