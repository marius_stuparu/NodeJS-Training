var _ = require('lodash');
var express = require('express');
var router = express.Router();

var connector = require('./connector');

router.get('/', function(req, res, next) {
    res.json(connector.db.findAll('branches'));
});

router.get('/city/:name', function(req, res, next) {
    var city_name = req.params.name;
    res.json(connector.db.find('branches', {'city': city_name}));
});

router.get('/:id/manager', function(req, res, next) {
    var branch_id = req.params.id;
    res.json(connector.db.find('employees', {'branch_id': branch_id, 'is_branch_manager': true}));
});

router.get('/:id', function(req, res, next) {
    var branch_id = req.params.id;
    res.json(connector.db.find('branches', {'id': branch_id}));
});

router.post('/', function(req, res, next) {
    var post_data = req.body;
    var constructed_object = {
        id: _.uniqueId('B1'),
        city: post_data.city,
        geolocation: [
            post_data.latitude,
            post_data.longitude
        ]
    }

    if( connector.db.insert('branches', constructed_object) ) {
        res.status(201).end();
    } else {
        res.status(500).end();
    }
});

module.exports = router;
